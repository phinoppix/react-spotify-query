This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

This is a small exercise to build a small app to search for music titles from Spotify.  Below are the primary objectives achieved by this application.

- App should load only the search bar 
- Search for artists from `https://api.spotify.com/v1/search?type=artist&limit=50&q=${query}`
- Only display those who have square images. Display in any fixed sizes as you like.
- No horizontal scrolling and responsive, images should fill up a row as much as possible before breaking off to next line.
- Just need to work on latest Chrome/Firefox/Edge.
- BONUS – add a button to export the metadata to CSV (placement and design are up to you) 
- BONUS – infinite scrolling 
  (I simply addedd a "load more" link button as the last tile of the result list, which will continue appending items returned by the $.artists.next url)

Get the code and install the Node modules:

```
$ cd your-workspace-folder

$ git code https://gitlab.com/phinoppix/react-spotify-query

$ cd react-spotify-query

$ npm i
```

To test the application:

```
$ npm run start
```