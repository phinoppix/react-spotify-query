import 'whatwg-fetch';

export const CACHE_QUERY_SPOTIFY = 'CACHE_QUERY_SPOTIFY';
export const CHANGE_QUERY_SPOTIFY = 'CHANGE_QUERY_SPOTIFY';

function execSpotifyQuery(url, isnext, dispatch) {

  fetch(url)
    .then(r => {
      if (r.status >= 200 && r.status < 300)
        return r;
      else
        throw new Error(r.statusText);
    })
    .then(r => {
      r.json().then(data => dispatch(cacheQuerySpotify(data, isnext)));
    });
}

export const querySpotify = (text) => (dispatch) => {
  if (text && text.length === 0) return;

  let qry = encodeURIComponent(text);
  execSpotifyQuery(`https://api.spotify.com/v1/search?type=artist&limit=50&q=${qry}`, false, dispatch);
};

export const nextQuerySpotify = (nextUrl) => (dispatch) => {
  execSpotifyQuery(nextUrl, true, dispatch);
}

const cacheQuerySpotify = (result, append = false) => ({
  type: CACHE_QUERY_SPOTIFY,
  append,
  result
});

export const changeQuerySpotify = (value) => ({
  type: CHANGE_QUERY_SPOTIFY,
  query: value
});

/* REDUCER */
var initAppState = {
  query: '',
  qryresult: {
    artists: {
      items: [],
      next: ''
    }
  }
};

export const appState = (state = initAppState, action) => {
  console.log(`** ACTION: ${action.type}`);
  if (action.type === CACHE_QUERY_SPOTIFY) {
    
    let qryresult = {
        artists: {
          items: action.result.artists.items,
          next: action.result.artists.next
        }
      };

    if (action.append) {
      qryresult.artists.items = state.qryresult.artists.items.concat(action.result.artists.items);
    }
    
    console.log(JSON.stringify({
      listcount: qryresult.artists.items.length,
      next: action.result.artists.next
    }));
    
    return {
      ...state,
      qryresult
    };
  }
  else if (action.type === CHANGE_QUERY_SPOTIFY) {
    return {
      ...state,
      query: action.query
    };
  }
  
  return state;
}
