import React, { Component } from 'react';
import MediaQuery from './media.query';
import MediaList from './media.list';
import MediaExport from './media.export';

class App extends Component {
  render() {
    const {appState, changeQuerySpotify, querySpotify, nextQuerySpotify} = this.props;
    let artists = appState.qryresult.artists;

    return (
      <div>
        <MediaQuery
          query={appState.query}
          changeQuerySpotify={changeQuerySpotify}
          querySpotify={querySpotify} />

        <MediaExport list={artists.items} />

        <MediaList 
          list={artists.items} 
          nextUri={artists.next}
          loadNext={nextQuerySpotify} />
      </div>
    );
  }
}

export default App;
