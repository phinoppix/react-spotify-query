import React, { Component } from 'react';
import PropTypes from 'prop-types';

// 0 variance is a perfect square.
const RATIO_VARIANCE = 0.05;

class MediaList extends Component {
	constructor(props, context) {
		super(props, context);
		this.callNext = this.callNext.bind(this);
	}

	callNext() {
		this.props.loadNext(this.props.nextUri);
	}

	isSquarish(width, height) {
		return Math.abs(width - height) / width <= RATIO_VARIANCE
	}

	render() {
		const { list } = this.props;
		let items = list.map(i => {
			let imgs200 = i.images.filter(img =>
				this.isSquarish(img.width, img.height));
			let imgurl = imgs200.length > 0 ? imgs200[0].url : '';

			return (
				<div key={i.id} className="mediatile">
					<img alt={i.name} src={imgurl} />
					<p>{i.name}</p>
				</div>);
		});

		let linkMore = null;
		if ((this.props.nextUri || '').length > 0) {
			linkMore =
				<div className="mediatile link-more">
					<p>
						<a href="javascript:void(0)"  //eslint-disable-line no-script-url
							onClick={this.callNext}>Load more...</a>
					</p>
				</div>;
		}

		return (
			<section className="medialist">
				{items}
				{linkMore}
			</section>
		);
	}
}

MediaList.propTypes = {
	list: PropTypes.arrayOf(PropTypes.object),
	nextUri: PropTypes.string,
	loadNext: PropTypes.func
}

export default MediaList;