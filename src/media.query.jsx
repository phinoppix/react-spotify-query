import React, { Component } from 'react';
import PropTypes from 'prop-types';

class MediaQuery extends Component {
	constructor(props, context) {
		super(props, context);
		this.onQueryChanged = this.onQueryChanged.bind(this);
		this.onKeyPress = this.onKeyPress.bind(this);
	}

	onQueryChanged(event) {
		this.props.changeQuerySpotify(event.target.value);
	}

	onKeyPress(event) {
		if (event.charCode === 13) {
			this.props.querySpotify(this.props.query);
		}
	}

	render() {
		return (
			<section className="mediaquery">
				<input name="query" placeholder="Enter some text to start query..."
					value={this.props.query}
					onChange={this.onQueryChanged}
					onKeyPress={this.onKeyPress} />
			</section>
		);
	}
}

MediaQuery.propTypes = {
	query : PropTypes.string.isRequired,
	changeQuerySpotify: PropTypes.func,
	querySpotify: PropTypes.func
};

export default MediaQuery;
