import { connect } from 'react-redux';
import App from './app';
import { querySpotify, changeQuerySpotify, nextQuerySpotify } from './dux';

var actions = {
	querySpotify,
	changeQuerySpotify,
	nextQuerySpotify
};

var states = ({ appState }) => ({
	appState
});

const AppContainer = connect(states, actions)(App);

export default AppContainer;