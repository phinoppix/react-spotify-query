import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import {appState} from './dux';

export function createAppStore() {
	var reducers = combineReducers({
		appState
	});

	var store = createStore(
		reducers,
		applyMiddleware(...[ thunk ])
	);

	return store;
}