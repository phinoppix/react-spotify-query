import React, { Component } from 'react';
import PropTypes from 'prop-types';

class MediaExport extends Component {

	constructor(props, context) {
		super(props, context);
		this.downloadCsv = this.downloadCsv.bind(this);
	}

	buildCSV() {
		var fields = 'name,uri';
		var csvrows = this.props.list.map(item => item.name + ',' + item.uri).join('\n');
		return fields + '\n' + csvrows;
	}

	downloadCsv() {
		var data = this.buildCSV();
		var csvData = new Blob([data], { type: 'text/csv;charset=utf-8;' });
		var filename = 'spotify.csv';

		if (navigator.msSaveBlob) {
			//IE11 & Edge
			navigator.msSaveBlob(csvData, filename);
		} else {
			var link = document.createElement('a');
			link.href = window.URL.createObjectURL(csvData);
			link.setAttribute('download', filename);
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
		}
	}


	render() {
		let btn;
		if (this.props.list.length > 0)
			btn = <button onClick={this.downloadCsv}>Export List to CSV...</button>;

		return (
			<div className="exportPanel">
				{btn}
			</div>
		);
	}
}

MediaExport.propTypes = {
	list: PropTypes.arrayOf(PropTypes.object)
}

export default MediaExport;