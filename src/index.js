import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import AppContainer from './app.container';
import {createAppStore} from './store';

var store = createAppStore();

ReactDOM.render(
	<Provider store={store}>
		<AppContainer />
	</Provider>
, document.getElementById('container'));
